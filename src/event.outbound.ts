export interface OutboundEvent {
	headers: {
		connectionIds: string[];
		userIds: string[];
	};
	type: string;
	content: any;
}
