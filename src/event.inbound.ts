export interface InboundEvent {
	headers: {
		taskId: string;
		connectionId: string;
		userId: string;
	};
	type: string;
	content: any;
}
