export interface PushNotificationMessage {
	userIds: string[];
	pushnotification: {
		title: string;
		body: string;
		icon: string;
		vibrate: number[];
		data: any;
		actions: { action: string; title: string }[];
		renotify: boolean;
		tag: string;
	};
}
