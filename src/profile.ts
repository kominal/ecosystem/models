import { AESEncrypted } from './aes.encrypted';

export interface Profile {
	cryptoState?: 'DECRYPTED';

	id: string;
	userId: string;
	displaynameHash: string;
	updatedAt: number;

	displayname: AESEncrypted;
	publicKey: AESEncrypted;
	avatar?: AESEncrypted;
	keyDisplayname: AESEncrypted;
	keyMasterEncryptionKey?: AESEncrypted;

	keyDecrypted?: string;
	publicKeyDecrypted?: ArrayBuffer;
	displaynameDecrypted?: string;
	avatarDecrypted?: any;
}
