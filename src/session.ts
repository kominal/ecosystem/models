import { AESEncrypted } from './aes.encrypted';

export interface Session {
	cryptoState?: 'VIEWABLE';

	_id: string;
	created: number;
	activityAt: number;

	device: AESEncrypted;

	deviceDecrypted?: string;
}
